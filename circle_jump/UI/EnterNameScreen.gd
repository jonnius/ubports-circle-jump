extends "res://UI/BaseScreen.gd"

var caps = true
var buttons = []

func ButtonPressed(button):
	var lineEdit = $MarginContainer/VBoxContainer/Keys/LineEdit
	if button.text == '←':
		lineEdit.text = lineEdit.text.substr(0, len(lineEdit.text)-1)
	elif button.text == 'CAPS':
		caps = false if caps else true
		
		for _button in buttons:
			var text = _button.text
			if len(text) == 1:
				_button.text = text.to_upper() if caps else text.to_lower()
	elif len(lineEdit.text) < lineEdit.max_length:
		lineEdit.text += button.text

func next():
	var lineEdit = $MarginContainer/VBoxContainer/Keys/LineEdit
	var score_data = {'name': lineEdit.text, 'score': $"/root/Main".score}
	$"/root/Main".highscores.append(score_data)
	$"/root/Main".save_score()
	$"/root/Main/Screens".game_over($"/root/Main".highscores.find(score_data))

func _ready():
	var label = $MarginContainer/VBoxContainer/Label
	var row1 = $MarginContainer/VBoxContainer/Keys/row1
	var row2 = $MarginContainer/VBoxContainer/Keys/row2
	var row3 = $MarginContainer/VBoxContainer/Keys/row3
	var row4 = $MarginContainer/VBoxContainer/Keys/row4
	var nextButton = $MarginContainer/VBoxContainer/VBoxContainer2/CenterContainer/Next

	var font = label.get_font("font").duplicate()
	font.size = 15

	for row in [
			['123456789←', row1],
			['abcdefghijklm', row2],
			['nopqrstuvwxyz', row3],
			[['CAPS'], row4]
		]:
		for key in row[0]:
			var button = ToolButton.new()
			
			button.text = key.to_upper() if caps else key
			button.add_font_override("font", font)
			button.connect("pressed", self, "ButtonPressed", [button])
			
			row[1].add_child(button)
			
			buttons.append(button)
	
	nextButton.connect("pressed", self, "next")
