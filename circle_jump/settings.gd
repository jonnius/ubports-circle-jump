extends Node

var score_file = "user://highscores.save"
var settings_file = "user://settings.save"
var enable_sound = false
var enable_music = false
var selected_theme = "NEON1"

var theme

var circles_per_level = 5

var color_schemes = {
	"NEON1": {
		'background': Color8(50, 50, 70),
		'player_body': Color8(203, 255, 0),
		'player_trail': Color8(204, 0, 255),
		'circle_fill': Color8(255, 0, 110), 
		'circle_static': Color8(0, 255, 102),
		'circle_limited': Color8(204, 0, 255)
	},
	"NEON2": {
		'background': Color8(0, 0, 0),
		'player_body': Color8(246, 255, 0),
		'player_trail': Color8(255, 255, 255),
		'circle_fill': Color8(255, 0, 110),
		'circle_static': Color8(151, 255, 48),
		'circle_limited': Color8(127, 0, 255)
	},
	"NEON3": {
		'background': Color8(76, 84, 95),
		'player_body': Color8(255, 0, 187),
		'player_trail': Color8(255, 148, 0),
		'circle_fill': Color8(255, 148, 0),
		'circle_static': Color8(170, 255, 0),
		'circle_limited': Color8(204, 0, 255)
	}
}

static func rand_weighted(weights):
	var sum = 0
	for weight in weights:
		sum += weight
	var num = rand_range(0, sum)
	for i in weights.size():
		if num < weights[i]:
			return i
		num -= weights[i]
		
var admob = null
#var real_ads = false
#var banner_top = false
## Fill these from your AdMob account:
#var ad_banner_id = ""
#var ad_interstitial_id = ""
var enable_ads = true setget set_enable_ads
var interstitial_rate = 0.2

func change_theme(button):
	selected_theme = button.text
	theme = color_schemes[selected_theme]
	save_settings()

	# Reinitialise to apply new colourscheme
	$"/root/Main"._ready()
	
func _ready():
	load_settings()
	theme = color_schemes[selected_theme]

	var container = $"/root/Main/Screens/SettingsScreen/MarginContainer/VBoxContainer/VBoxContainer"
	var label = $"/root/Main/Screens/SettingsScreen/MarginContainer/VBoxContainer/VBoxContainer/Label"
	var font = label.get_font("font").duplicate()
	font.size = 20

	for scheme in color_schemes:
		var button = ToolButton.new()
		
		button.text = scheme
		button.add_font_override("font", font)
		button.connect("pressed", self, "change_theme", [button])
		
		container.add_child(button)

func set_enable_ads(value):
	enable_ads = value
	if enable_ads:
		admob.show_banner()
	if !enable_ads:
		admob.hide_banner()
	save_settings()
		
func save_settings():
	var f = File.new()
	f.open(settings_file, File.WRITE)
	f.store_var(enable_sound)
	f.store_var(enable_music)
	f.store_var(selected_theme)
	f.close()
	
func load_settings():
	var f = File.new()
	if f.file_exists(settings_file):
		f.open(settings_file, File.READ)
		enable_sound = f.get_var()
		enable_music = f.get_var()
		selected_theme = f.get_var()
		f.close()
